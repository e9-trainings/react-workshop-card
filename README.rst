===================
React Card tutorial
===================

This project provides the React tutorial scaffolding. It's enough that you clone this repository with::

	$ git clone git@gitlab.com:evonove/react-workshop.git

then initialize your own repository::

	$ cd react-workshop
	$ rm -rf .git/
	$ git init
	$ git add .
	$ git commit -m 'initial commit'

Getting started
---------------

The tutorial is distributed with the following structure::

	.
	├── README.rst
	└── client
    	├── index.html
    	└── js
        	└── card.js

During the workshop, you will update the ``index.html`` and the ``card.js`` files. Everything else
is already provided through a CDN, so no builders or bundlers are required.

Launching the server
~~~~~~~~~~~~~~~~~~~~

There is no a built-in server so the fastest way to launch your React application is using ``python``.
From the ``client/`` folder, simply::

	$ python2 -m SimpleHTTPServer  # Python 2

or::

	$ python3 -m http.server  # Python 3

You can open your front end application at the `http://localhost:8000`_ URL.

Happy **coding**!

.. _http://localhost:8000: http://localhost:8000

What we're doing
----------------

Write a Material Design `Card component`_, so that developers can set the following attributes:

* A title
* A description
* Background image (optional attribute)
* One action button (optional attribute)

The following is an example with all optional attributes set: http://bit.ly/29AHuIV

**NOTE:** providing beautiful styles **is not** a requirement.

.. _Card component: https://material.google.com/components/cards.html

Public API
----------

Cards should be create using an internal data structure such as:

.. code-block:: javascript

	var cards = [
		{
			id: 1,
			title: "Kangaroo Valley",
			description: "Located two hours...",
			image: "http://lorempixel.com/400/237/nature/2/",
			action: { text: "Go to website", target: "https://evonove.it" }
		},
	]

**BONUS:** let developers create one or more action buttons in the same card.